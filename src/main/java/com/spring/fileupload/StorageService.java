package com.spring.fileupload;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public class StorageService {
	
	private String mesaj;
	
	
	
	public boolean store(MultipartFile fileToBeUploaded) {
		
		try {
						
			byte[] bbb = fileToBeUploaded.getBytes();
			
			/*
			int i;
			for(i=0;i<bbb.length;i++) {
			System.out.println(bbb[i]);	}
			File[] roots = File.listRoots( );
			File dir = new File(roots[0], "Users");
			File dir1 = new File(dir, "marin.abaluta");
			File dir2 = new File(dir1, "Desktop");
			File dir3 = new File(dir2, "Test");*/
			
			String uploadRootPath = "C:\\Users\\marin.abaluta\\Desktop\\Test";
			String name = fileToBeUploaded.getName();
			
			if (name != null && name.length() > 0) {
				
				File serverFile = new File(uploadRootPath,name);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bbb);
	            stream.close();
	            System.out.println("Write file: " + serverFile);
	            this.mesaj = "uploadResultOk";
	            return true;
	            
			}
			this.mesaj = "uploadResultFail";
			return false;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}



	public String getMesaj() {
		return mesaj;
	}



	public void setMesaj(String mesaj) {
		this.mesaj = mesaj;
	}
	
	

}
