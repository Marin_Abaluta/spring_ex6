package com.spring.fileupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercitiul6Application {

	public static void main(String[] args) {
		SpringApplication.run(Exercitiul6Application.class, args);
	}
}
