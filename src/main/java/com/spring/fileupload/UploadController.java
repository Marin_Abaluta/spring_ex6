package com.spring.fileupload;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class UploadController {
	

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showForm() {
		return "uploadForm";
	}
	
	
	@PostMapping(value = "/")
	public String doUpload(@RequestParam ("upload") MultipartFile fileFromForm) {
		
		StorageService ss = new StorageService();
		ss.store(fileFromForm);
		return ss.getMesaj();
//		return "uploadResultOk";
	}

}
